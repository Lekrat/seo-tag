<?php

namespace Lekrat\SeoTag;
use Lekrat\SeoTag\Classes\Helper;

/**
 * Class Img
 *
 * @package Lekrat\SeoTag
 */
class Img
{
    /**
     * @param array $tagHtmlAttributes
     * @return string
     */
    public static function getTag($tagHtmlAttributes = [])
    {
        if (\trim($tagHtmlAttributes['alt']) === ''){
            $tagHtmlAttributes['alt'] = \pathinfo($tagHtmlAttributes['src'])['filename'];
        }
        
        $tagHtmlAttributes['alt'] =  \htmlentities(\strip_tags($tagHtmlAttributes['alt']), ENT_QUOTES, 'utf-8');

        return Helper::buildHtmlContent('', $tagHtmlAttributes, 'img', false);
    }
}