<?php

namespace Lekrat\SeoTag;

/**
 * Class Config
 *
 * @package Lekrat\SeoTag
 */
class Config
{
    protected static $debugEnabled = false;
    protected static $headerH1CheckEnabled = true;

    public static function enableDebug(){
        self::$debugEnabled = true;
    }

    public static function disableHeaderH1Check()
    {
        self::$headerH1CheckEnabled = false;
    }

    public static function enableHeaderH1Check()
    {
        self::$headerH1CheckEnabled = true;
    }

    /**
     * @return bool
     */
    public static function isDebugEnabled()
    {
        return self::$debugEnabled;
    }

    /**
     * @return bool
     */
    public static function isHeaderH1CheckEnabled()
    {
        return self::$headerH1CheckEnabled;
    }
}