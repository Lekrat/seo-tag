<?php

namespace Lekrat\SeoTag;
use Lekrat\SeoTag\Classes\Helper;

/**
 * Class Meta
 *
 * @package Lekrat\SeoTag
 */
class Meta
{
    const TITLE_MAX_LENGTH = 60;
    const DESCRIPTION_MAX_LENGTH = 155;
    const DEFAULT_CHARSET = 'utf-8';

    /**
     * @param string $name
     * @param string $content
     * @return string
     */
    public static function getTag($name = '', $content = '')
    {
        $tagHtmlAttributes = [
            'name' => $name,
            'content' => Helper::htmlEncode(Helper::cutText($content, self::DESCRIPTION_MAX_LENGTH)),
        ];

        return Helper::buildHtmlContent('', $tagHtmlAttributes, 'meta', false);
    }

    /**
     * @param string $text
     * @return string
     */
    public static function getTitleTag($text = '')
    {
        return Helper::buildHtmlContent(Helper::htmlEncode(Helper::cutText($text, self::TITLE_MAX_LENGTH)), [], 'title');
    }

    /**
     * @param string $text
     * @return string
     */
    public static function getDescriptionTag($text = '')
    {
        $tagHtmlAttributes = [
            'name' => 'description',
            'content' => Helper::htmlEncode(Helper::cutText($text, self::DESCRIPTION_MAX_LENGTH)),
        ];

        return Helper::buildHtmlContent('', $tagHtmlAttributes, 'meta', false);
    }

    /**
     * @param string $text
     * @return string
     */
    public static function getKeyWordsTag($text = '')
    {
        $tagHtmlAttributes = [
            'name' => 'keywords',
            'content' => Helper::htmlEncode(\trim(\strip_tags($text))),
        ];

        return Helper::buildHtmlContent('', $tagHtmlAttributes, 'meta', false);
    }

    /**
     * @param string $charset
     * @return string
     */
    public static function getCharsetTag($charset = self::DEFAULT_CHARSET)
    {
        $tagHtmlAttributes = [
            'charset' => \trim(\strip_tags($charset)),
        ];

        return Helper::buildHtmlContent('', $tagHtmlAttributes, 'meta', false);
    }

    /**
     * @param bool $index
     * @param bool $follow
     * @param bool $archive
     * @param bool $imageIndex
     * @param bool $snippet
     * @param bool $pageDescription
     * @param bool $pageDescriptionInYahooDir
     * @return string
     */
    public static function getRobotsTag($index = true, $follow = true, $archive = true, $imageIndex = true, $snippet = true, $pageDescription = true, $pageDescriptionInYahooDir = true)
    {
        $r = [];

        $r[] = $index === true ? 'index' : 'noindex';
        $r[] = $follow === true ? 'follow' : 'nofollow';

        if (!$archive){
            $r[] = 'noarchive';
            $r[] = 'nocache';
        }

        if (!$imageIndex){
            $r[] = 'noimageindex';
        }

        if (!$snippet){
            $r[] = 'nosnippet';
        }

        if (!$pageDescription){
            $r[] = 'noodp';
        }

        if (!$pageDescriptionInYahooDir){
            $r[] = 'noydir';
        }


        $tagHtmlAttributes = [
            'name' => 'robots',
            'content' => \implode(',', $r),
        ];

        return Helper::buildHtmlContent('', $tagHtmlAttributes, 'meta', false);
    }
}