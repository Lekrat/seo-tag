<?php

namespace Lekrat\SeoTag\Classes;

/**
 * Class Stack
 *
 * @package Lekrat\SeoTag
 */
class Stack
{
    const MAX_STACK_LEVEL = 6;

    const DEFAULT_NAME = 'main';
    const DEFAULT_START_INDEX = 1;

    public $name = '';
    public $currentIndex = 1;
    public $initialIndex = 1;
    public $parentStackName = '';

    /**
     * Stack constructor.
     *
     * @param string $name
     * @param int $startIndex
     */
    public function __construct($name = self::DEFAULT_NAME, $startIndex = self::DEFAULT_START_INDEX, $parentStackName = '')
    {
        $this->name = $name;

        if ($startIndex < 1){
            $startIndex = self::DEFAULT_START_INDEX;

        } elseif($startIndex > self::MAX_STACK_LEVEL){
            $startIndex = self::MAX_STACK_LEVEL;
        }

        $this->currentIndex = $startIndex;
        $this->initialIndex = $startIndex;
        $this->parentStackName = $parentStackName;
    }

    /**
     * @return int
     */
    public function getNextIndex()
    {
        $index = $this->currentIndex;
        if ($this->currentIndex < self::MAX_STACK_LEVEL){
            ++$this->currentIndex;
        }

        return $index;
    }
}