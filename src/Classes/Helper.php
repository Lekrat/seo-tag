<?php

namespace Lekrat\SeoTag\Classes;

use Lekrat\SeoTag\Config;

/**
 * Class Helper
 *
 * @package Lekrat\SeoTag\Classes
 */
class Helper
{
    /**
     * @param array $attributes
     * @return string
     */
    public static function convertAttributesToString($attributes = [])
    {
        $r = [];
        foreach ($attributes as $attribute=>$value){
            $val = trim($value);
            if ($val === ''){
                $r[] = "{$attribute}";

            } else {
                $r[] = "{$attribute}=\"{$value}\"";
            }
        }

        return \trim(\implode(' ', $r));
    }

    /**
     * @param string $content
     * @param array $tagHtmlAttributes
     * @param string $tag
     * @param bool $autoClose
     * @return string
     */
    public static function buildHtmlContent($content = '', $tagHtmlAttributes = [], $tag = '', $autoClose = true)
    {
        $content = \trim($content);

        if (Config::isDebugEnabled()){
            $tagHtmlAttributes['data-seo-tag'] = '';
        }

        $attributes = self::convertAttributesToString($tagHtmlAttributes);
        if ($attributes !== ''){
            $attributes = " {$attributes}";
        }
        
        if ($autoClose === true) {
            $r = "<{$tag}{$attributes}>{$content}</{$tag}>";
        } elseif($tag === 'meta'){
            $r = "<{$tag}{$attributes}>";
        } else {
            $r = "<{$tag}{$attributes}/>";
        }

        return $r;
    }

    /**
     * @param string $string
     * @param int $length
     * @return bool|string
     */
    public static function cutText($string = '', $length = 60)
    {
        $string = \trim(\strip_tags($string));

        return \substr($string, 0, $length);
    }

    /**
     * @param string $text
     * @param string $charset
     * @return string
     */
    public static function htmlEncode($text = '', $charset = 'utf-8')
    {
        return \htmlentities($text, ENT_QUOTES, $charset);
    }
}