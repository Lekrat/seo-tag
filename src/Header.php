<?php

namespace Lekrat\SeoTag;
use Lekrat\SeoTag\Classes\Helper;
use Lekrat\SeoTag\Classes\Stack;

/**
 * Class Header
 *
 * @package Lekrat\SeoTag
 */
class Header
{
    private static $HAS_H1_LOADED = false;

    /** @var Stack[] */
    private static $STACKS = [];

    /**
     * @param string $text
     * @param array $tagHtmlAttributes
     * @param string $stackName
     * @param string $parentStackName
     * @param int $startIndexFrom
     * @return string
     */
    public static function getTag($text = '', $tagHtmlAttributes = [], $stackName = Stack::DEFAULT_NAME, $parentStackName = '', $startIndexFrom = 0)
    {
        $stackStartIndex = 1;
        $parentStack = self::getStack($parentStackName);
        if (is_object($parentStack) && !is_object(self::getStack($stackName))){
            $stackStartIndex = $parentStack->currentIndex;
        }

        if ($startIndexFrom > 0){
            $stackStartIndex = $startIndexFrom;
        }

        self::addStack($stackName, $stackStartIndex, $parentStackName);
        $stack = self::getStack($stackName);

        $text = \trim($text);

        if (\is_object($stack)){
            $tag = "h{$stack->getNextIndex()}";
            if (Config::isDebugEnabled()){
                $tagHtmlAttributes['data-seo-tag-stack-name'] = $stack->name;
                $tagHtmlAttributes['data-seo-tag-stack-parent-name'] = $parentStackName;
            }
            return Helper::buildHtmlContent($text, $tagHtmlAttributes, $tag);
        }

        return $text;
    }

    /**
     * @param string $name
     * @param int $startIndex
     * @param string $parentStackName
     * @return bool
     */
    private static function addStack($name = Stack::DEFAULT_NAME, $startIndex = Stack::DEFAULT_START_INDEX, $parentStackName = '')
    {
        if (isset(self::$STACKS[$name])){
            return false;
        }

        $name = \trim($name);

        if (self::$HAS_H1_LOADED === false && $startIndex !== 1 && Config::isHeaderH1CheckEnabled()){
            $startIndex = 1;
        }

        if ($startIndex === 1 && Config::isHeaderH1CheckEnabled()){
            if (self::$HAS_H1_LOADED === true){
                $startIndex = Stack::DEFAULT_START_INDEX;
                ++$startIndex;
            } else {
                self::$HAS_H1_LOADED = true;
            }
        }

        self::$STACKS[$name] = new Stack($name, $startIndex, $parentStackName);

        return true;
    }

    /**
     * @param string $name
     * @return Stack
     */
    private static function getStack($name = Stack::DEFAULT_NAME)
    {
        return self::$STACKS[$name];
    }
}